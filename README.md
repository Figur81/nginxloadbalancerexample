# Nginx load balancer(no ingress) example
This repository show how to use nginx like a load balancer type. 
<p align="center">
  <img src="https://i.pinimg.com/originals/41/a8/06/41a806ab9fa9ce651308656f31bc2023.png">
</p>

## What's load balancer?


## Look this example:
[![asciicast](https://asciinema.org/a/UzwYHvaHIzAOwzoXCViMN9A0m.svg)](https://asciinema.org/a/UzwYHvaHIzAOwzoXCViMN9A0m)

Lets undestand this terminal sequence!

Creating load balancer service and pods:
```console
microk8s kubectl create -f nginxpod1.yml
microk8s kubectl create -f nginxpod2.yml
microk8s kubectl create -f nginx-load-balancer-service.yml
```
Getting services and pods to show information:
```console
watch microk8s kubectl get services
watch microk8s kubectl get pods
```
Getting pod logs to show incoming requests
```console
watch kubectl logs nginx1
watch kubectl logs nginx2
```
Requesting load balance to show load balance behavior and delete a pod to see load balance autoConfig
```console
curl http://<load-balancer-ip>:<load-balancer-port>
kubectl delete pod <balanced pod>
curl http://<load-balancer-ip>:<load-balancer-port>
```
